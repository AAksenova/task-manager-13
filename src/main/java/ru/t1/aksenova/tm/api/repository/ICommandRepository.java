package ru.t1.aksenova.tm.api.repository;

import ru.t1.aksenova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
